import os
import json
import numpy as np
from scipy.spatial.transform import Rotation
from tqdm import tqdm

json_dir = "/media/ml_server/2022-08/sample_data/JSON"
data_dir = "/media/ml_server/2022-08/sample_data"
combined_lidar_dir = "/media/ml_server/2022-08/sample_data/Log_16_08_2022/LidALL"
json_files = os.listdir(json_dir)

def x2R(x):
    trans = [x[0], x[1], x[2], 1.0]
    R = np.eye(4)
    rotation = Rotation.from_euler("xyz", [x[3], x[4], x[5]], degrees=False)
    R[:3, :3] = rotation.as_matrix()
    R[:, 3] = trans
    return R


def transform(points, R):
    points[:, :3] = np.hstack((points[:, :3], np.ones([points.shape[0], 1]))) @ R.T[:, :3]
    return points

def combine_points(point_dict, calib_extrinsic):
    points_list = []
    for name, points in point_dict.items():    
        R = calib_extrinsic[name]
        points_list.append(transform(points, R))

    points = np.concatenate(points_list, axis=0)
    points = points.astype(np.float32).reshape(-1, 4)
    return points  


calib_path = os.path.join("../calibration/extrinsic.json")
with open(calib_path, "r") as file:
    calib_extrinsic = json.load(file)
    
for name, x in calib_extrinsic.items():
    calib_extrinsic[name] = x2R(x)


# for json_file in json_files:
#     scene = json_file.split('.')[0]
#     json_path = os.path.join(json_dir, json_file)
#     with open(json_path) as f:
#         samples = json.load(f)
#         for sample in samples:
#             # print(sample['timestamp'])
#             timestamp = sample['timestamp']
#             points_dicts = {}
#             sample_lidar = sample['lidar']
#             for lidar, path in sample_lidar.items():
#                 data_path = os.path.join(data_dir, path)
#                 points = np.fromfile(data_path, dtype=np.float32).reshape(-1, 4)
#                 points_dicts[lidar] = points
            
#             points = combine_points(points_dicts, calib_extrinsic)
#             combined_lidar_path = os.path.join(combined_lidar_dir, str(timestamp) + '.bin')
#             points.tofile(combined_lidar_path)
#             tqdm.write(combined_lidar_path)



for json_file in tqdm(json_files):
    json_path = os.path.join(json_dir, json_file)
    with open(json_path, "r") as f:
        samples = json.load(f)

    for sample in samples:
        timestamp = sample['timestamp']
        combined_lidar_path = os.path.join("Log_16_08_2022/LidALL", str(timestamp) + '.bin')
        sample['lidar']["LidALL"] = combined_lidar_path

    with open(json_path, "w") as f:
        json.dump(samples, f, indent=4)
    tqdm.write(json_path)