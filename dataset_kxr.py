
import numpy as np
import os, json, pickle
import json
import cv2
import pickle 
import sys
from scipy.spatial.transform import Rotation
from pathlib import Path
from tqdm import tqdm
class Dataset:	
    def __init__(self, data_root, info_root, pts2img=False):
        self.info_dir = info_root
        self.data_dir = data_root
        self.pts2img = pts2img
        print(self.info_dir)
        info_names = os.listdir(self.info_dir)
        info_names = sorted(info_names)
        self.infos = [] 
        for info_name in info_names:
            
            info_path = os.path.join(self.info_dir, info_name)
            with open(info_path, 'rb') as f:
                infos = pickle.load(f)
                print(info_path)
                self.infos.append(infos)

        self.infos = np.concatenate(self.infos, axis=0)
        print(f"{len(self.infos)} frames loaded")
        mod_path = os.path.dirname(__file__)
        calib_path = os.path.join(mod_path, "calibration/extrinsic.json")
        with open(calib_path, "r") as file:
            calib_extrinsic = json.load(file)

        self.calib_extrinsic = {}
        for name, x in calib_extrinsic.items():
            self.calib_extrinsic[name] = self.x2R(x)
        
        calib_path = os.path.join(mod_path, "calibration/intrinsic.json")
        with open(calib_path, "r") as f:
            self.calib_intrinsic = json.load(f)


    def __getitem__(self, idx):
        timestamp = self.get_timestamp(idx)
        points = self.get_points(idx)
        images = self.get_images(idx)
        images = self.undistort(images, self.calib_intrinsic)
        if self.pts2img:
            images = self.pts_to_img(images, points, self.calib_extrinsic, self.calib_intrinsic)

        annos = self.get_annotations(idx)
        sample = {
            "timestamp":timestamp,
            "points" : points,
            "images": images,
            "annos": annos
        }
        return sample

    def get_points(self, idx):
        info = self.infos[idx]
        points_dicts = None
        if 'lidar' in info:
            points_dicts = {}
            for message, data_path in info["lidar"].items():
                # message = name_dict[message]
                data_path = os.path.join(self.data_dir, data_path)
                points = np.fromfile(data_path, dtype=np.float32).reshape(-1, 4) # x, y, z, i
                
             
                points_dicts[message] = points
       
        points = self.combine_points(points_dicts, self.calib_extrinsic)

        return points

    def get_all_points(self, idx):
        info = self.infos[idx]
        points_dicts = None
        if 'lidar' in info:
            points_dicts = {}
            for message, data_path in info["lidar"].items():
                # message = name_dict[message]
                data_path = os.path.join(self.data_dir, data_path)
                points = np.fromfile(data_path, dtype=np.float32).reshape(-1, 4) # x, y, z, i
                points_dicts[message] = points
       
        points = self.combine_points(points_dicts, self.calib_extrinsic)

        return points

    def get_images(self, idx):
        info = self.infos[idx]
        images_dict = None
        if 'camera' in info:
            images_dict = {}
            for message, data_path in info["camera"].items():
                # message = name_dict[message]
                data_path = os.path.join(self.data_dir, data_path)
                images_dict[message] = cv2.imread(data_path)
        return images_dict

    def get_timestamp(self, idx):
        return self.infos[idx]["timestamp"]

    def get_annotations(self, idx):
        info = self.infos[idx]
        annos = None
        if 'annos' in info:
            annos = info['annos']
        return annos

    def transform(self, points, R):
        points[:, :3] = np.hstack((points[:, :3], np.ones([points.shape[0], 1]))) @ R.T[:, :3]
        return points

    def x2R(self, x):
        """
        Function to compute a projection matrix from projection vector, x

        Args:
            x (ndarray): Projection Vector. Shape (6,)

                x[0]: Translation in x
                x[1]: Translation in y
                x[2]: Translation in z
                x[3]: Rotation in x, Euler Angle
                x[4]: Rotation in y, Euler Angle
                x[5]: Rotation in z, Euler Angle

        Returns:
            R (ndarray): 4x4 Projection Matrix
        """
        trans = [x[0], x[1], x[2], 1.0]
        R = np.eye(4)
        rotation = Rotation.from_euler("xyz", [x[3], x[4], x[5]], degrees=False)
        R[:3, :3] = rotation.as_matrix()
        R[:, 3] = trans
        return R

    def combine_points(self, point_dict, calib_extrinsic):
        points_list = []
        for name, points in point_dict.items():    
            R = calib_extrinsic[name]
            points_list.append(self.transform(points, R))
    
        points = np.concatenate(points_list, axis=0)
        points = points.astype(np.float32).reshape(-1, 4)
        return points  


    def undistort(self, images, calib_intrinsic):
        images_ret = {}
        for name, image in images.items():
            h, w = image.shape[:2]
            K = np.asarray(calib_intrinsic[name + "_K"])
            D = np.asarray(calib_intrinsic[name + "_D"])
		
            if '60' in name:
                new_K, _ = cv2.getOptimalNewCameraMatrix(K, D, (w,h), 1, (w,h))
                map1, map2 = cv2.initUndistortRectifyMap(K, D, None, new_K, (w,h), 5)
            else:
                new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(K, D, (w, h), np.eye(3), balance=0.6)
                map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), new_K, (w, h), cv2.CV_16SC2)

            image_ret = cv2.remap(image, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
            images_ret[name] = image_ret
            calib_intrinsic[name + "_new_K"] = new_K

        return images_ret

    def pts_to_img(self, images, points, calib_extrinsic, calib_intrinsic):
        images_ret = {}
        for name, image in images.items():
            h, w, c = image.shape
            R = calib_extrinsic[name]
            R = np.linalg.inv(np.asarray(R))
            points_cam = np.hstack((points[:, :3], np.ones([points.shape[0], 1]))) @ R.T[:, :3]
            new_K = calib_intrinsic[name + "_new_K"]
            for point in points_cam:
                proj = new_K.dot(point[:3])
                if proj[2] <= 0.1:
                    continue
                px, py = int(proj[0] / proj[2]), int(proj[1] / proj[2])
                if (0 <= px < w) and (0 <= py < h):
                    image = cv2.circle(image, (px, py), radius=0, color=(0, 255, 255), thickness=3)
            images_ret[name] = image

        return images_ret

        
if __name__ == '__main__':

	info_dir = sys.argv[1]
	data_dir = sys.argv[2]
	dataset = Dataset(data_dir, info_dir, pts2img=True)
    
	for sample in tqdm(dataset):
		print(f"timestamp: {sample['timestamp']}")
		print(f"points shape: {sample['points'].shape}")

		for name, image in sample['images'].items():
		 	print(name, image.shape)
    	
    	
    	

