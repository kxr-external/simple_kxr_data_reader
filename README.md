# This repo is a simple data reader for kxr dataset

# 1. Clone this repo
```git clone git@gitlab.com:kxr-ml/simple_kxr_data_reader.git```

# 2. Enter simple_kxr_data_reader and convert json file to pkl file that is readable by dataset_kxr.py
 ```python create_info_kxr.py```

infos are generated under PATH_TO_REPO/simple_kxr_data_reader/info

# 3. Please contact the us to get the data and symlink to this repo and organize the folder like PATH_TO_REPO/simple_kxr_data_reader/data
 
# 4. Run the dataset_kxr.py to read the data
```python dataset_kxr2.py /media/ml_server/2022-08/sample_data /media/ml_server/2022-08/sample_data/JSON ```
/media/ml_server/2022-08/sample_data is your dataset root and /media/ml_server/2022-08/sample_data/JSON is your info root

