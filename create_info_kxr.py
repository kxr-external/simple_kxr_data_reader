import os
import json
import numpy as np
import pickle
import sys


CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))

json_files = os.listdir("JSON")

for json_file in json_files:
	json_path = os.path.join("JSON", json_file)	
	json_name = json_file.split('.')[0]

	with open(json_path, 'r') as f:
		json_info = json.load(f)
		infos = []
		for frame in json_info:
		    print(frame['timestamp'])
		    infos.append(frame)
		infos_path = os.path.join("info", f"{json_name}" + '.pkl')
		with open(infos_path, 'wb') as f:
		    pickle.dump(infos, f)
